//
//  ReviewsView.swift
//  
//
//  Created by Juleanny Navas on 01/11/2022.
//

import SwiftUI
import Kingfisher
import Model

public struct ReviewsView: View {
    
    @State var movie: Movie
    @State var reviews: ReviewResponse?
    
    public var body: some View {
        VStack() {
            HStack {
                RoundedRectangle(cornerRadius: 24)
                    .frame(width: 72, height: 72)
                    .foregroundColor(.gray)
                    .overlay (
                        KFImage(URL(string: "https://image.tmdb.org/t/p/w500/\(movie.image!)"))
                            .resizable()
                            .foregroundColor(.black)
                            .cornerRadius(24))
                            .padding(.horizontal, 20)

                Text(movie.name)
                    .font(.headline)
                Spacer()
            }
            .padding(.vertical, 8)
            Spacer()
            VStack {
                if let reviews = reviews?.results {
                    if reviews.count != 0 {
                        List {
                            ForEach(reviews, id: \.self) { review in
                                Text("Author: \(review.author)")
                                    .font(.headline)
                                Text(review.content)                       }
                            .font(.body)
                            .listRowSeparator(.hidden)
                        }
                        .listStyle(.plain)
                    } else {
                        Spacer()
                        Text("This film has not been reviewed yet")
                        Spacer()
                    }
                } else {
                    Spacer()
                    Text("This film has not been reviewed yet")
                    Spacer()
                }
            }
        }
    }
}

extension ReviewsView {
    
    public static func viewRepresentation(viewController: UIViewController, movie: Movie, reviews: ReviewResponse?) -> UIHostingController<ReviewsView> {
        let childView = UIHostingController(rootView: ReviewsView(movie: movie, reviews: reviews))
        viewController.view.addSubview(childView.view)
        childView.didMove(toParent: viewController)
        return childView
    }
    
}

struct ReviewsView_Previews: PreviewProvider {
    static var previews: some View {
        ReviewsView(movie: Movie(name: "Unnamed", description: "Undescription", movieID: 0))
    }
}
