import UIKit

@objc protocol TvShowsCleanRoutingLogic
{
    func routeToTvShowDetail(segue: UIStoryboardSegue?)
}

protocol TvShowsCleanDataPassing
{
    var dataStore: TvShowsCleanDataStore? { get }
}

class TvShowsCleanRouter: NSObject, TvShowsCleanRoutingLogic, TvShowsCleanDataPassing
{
    weak var viewController: TvShowsCleanViewController?
    var dataStore: TvShowsCleanDataStore?
    
    // MARK: Routing
    
    func routeToTvShowDetail(segue: UIStoryboardSegue?)
    {
        if let segue = segue {
            let destinationVC = segue.destination as! TvShowsDetailsCleanViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToTvShowDetail(source: dataStore!, destination: &destinationDS)
        } else {
            let storyboard = UIStoryboard(name: "TVshowsStoryboard", bundle: nil)
            let destinationVC = storyboard.instantiateViewController(withIdentifier: "TvShowsDetailsCleanViewController") as! TvShowsDetailsCleanViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToTvShowDetail(source: dataStore!, destination: &destinationDS)
            navigateToTvShowDetail(source: viewController!, destination: destinationVC)
        }
    }
    
    // MARK: Navigation
    
    func navigateToTvShowDetail(source: TvShowsCleanViewController, destination: TvShowsDetailsCleanViewController)
    {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToTvShowDetail(source: TvShowsCleanDataStore, destination: inout TvShowsDetailsCleanDataStore)
    {
        destination.selectedTvShow = source.selectedTvShow
    }
}
