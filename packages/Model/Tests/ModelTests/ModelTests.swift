import XCTest
@testable import Model

final class ModelTests: XCTestCase {
    
    var mediaModel: Media!
    var theMovie: Media!
    var tvShow: TvShow!

    override func setUp() {
        mediaModel = Media(name: "Enola Holmes 2", title: "Enola Holmes 2", description: "TvShow")
        tvShow = TvShow(name: "Wandavision", description: "Wandavision", image: "test/imagr", rating: 4)
    }
    
    override func tearDown() {
        mediaModel = nil
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Model().text, "Hello, World!")
    }

    func testMediaNameAndTitleNotEmpty() {
        XCTAssertFalse(mediaModel.name?.isEmpty == nil)
        XCTAssertTrue(mediaModel.title != nil)
    }
    
    func testMediaImageReturnsDefaultImage() {
        XCTAssertEqual(mediaModel.imageURLRepresentation(), URL(string: "https://media.istockphoto.com/vectors/error-page-or-file-not-found-icon-vector-id924949200?k=20&m=924949200&s=170667a&w=0&h=-g01ME1udkojlHCZeoa1UnMkWZZppdIFHEKk6wMvxrs="))
    }
    
    func testMediaDictionaryRepresentationNotNil() {
        let mediaDictionaryRepresentation = [
            "name": "Enola Holmes 2",
            "title": "Enola Holmes 2",
            "description": "TvShow",
            "imageURL": "",
        ]
        XCTAssertFalse(mediaModel.dictionaryRepresentation == nil)
    }
    
    // MARK: Movie and TVShow Tests
    
    func testNameIsMovie() {
        theMovie = Media(name: "A Movie", title: nil, description: "Movie")
        XCTAssertTrue(theMovie.name != nil)
        XCTAssertTrue(theMovie.title == nil)
    }
    
    func testNameIsTVShow() {
        theMovie = Media(name: nil, title: "WandaVision", description: "Tv Show")
        XCTAssertTrue(theMovie.name == nil)
        XCTAssertTrue(theMovie.title != nil)
    }
    
    // MARK: TVShows Tests
    
    func testTvShowNotNil() {
        XCTAssertNotNil(tvShow)
    }
    
    func testTvShowDictionaryReprentation() {
        let dictionaryReprentation = [
            "name": "Wandavision",
            "about": "Wandavision",
            "rating": "4.0",
            "imageURL": "test/imagr"
        ]
        XCTAssertEqual ( tvShow.dictionaryRepresentation, dictionaryReprentation)
    }
    
    func testTvShowImageRepresentationContainsCorrectImageURL() {
        XCTAssertEqual(tvShow.imageURLRepresentation(), URL(string: "https://image.tmdb.org/t/p/w500/test/imagr"))
    }
    
    func testTvShowImageRepresentationReturnsDefaultImage() {
        tvShow = TvShow(name: "Wandavision", description: "Wandavision", rating: 4)
        XCTAssertEqual(tvShow.imageURLRepresentation(), Constants.defaultImageURL)
    }
    
    func testTvShowModelMovieDetails(){
        XCTAssertEqual(tvShow.movieDetails(age: 18), "")
    }
    
    func testMovieAndActorShouldHaveTheSameDefaultImage() {
        let actor = Actor(name: "Bryan", popularity: 1, id: 1)
        let movie = Movie(name: "Jurassic Park", description: "Giant animals", movieID: 1)
        XCTAssertEqual(actor.imageURLRepresentation(), movie.imageURLRepresentation())
        XCTAssertEqual(actor.imageURLRepresentation(), Constants.defaultImageURL)
        XCTAssertEqual(movie.imageURLRepresentation(), Constants.defaultImageURL)
    }
        
    func testMovieDescriptionShouldReturnAnDefaultMessageWhenIsEmpty() {
        let movie = Movie(name: "", description: "", movieID: 1)
        XCTAssertEqual(movie.movieDetails(), Constants.emptyDescriptionMessage)
    }
}
