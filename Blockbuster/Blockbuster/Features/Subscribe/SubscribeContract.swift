//
//  SubscribeContract.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 16/11/22.
//  
//

import UIKit

// MARK: View Output (Presenter -> View)
protocol PresenterToViewSubscribeProtocol {
    func showSubscriptionDetails(status: Bool)
}

// MARK: View Input (View -> Presenter)
protocol ViewToPresenterSubscribeProtocol {
    
    var view: PresenterToViewSubscribeProtocol? { get set }
    var interactor: PresenterToInteractorSubscribeProtocol? { get set }
    var router: PresenterToRouterSubscribeProtocol? { get set }
    
    func requestSubscription(with subscriptionData: SubscriptionData)
}

// MARK: Interactor Input (Presenter -> Interactor)
protocol PresenterToInteractorSubscribeProtocol {
    
    var presenter: InteractorToPresenterSubscribeProtocol? { get set }
    
    func requestSubscription(with subscriptionData: SubscriptionData)
}


// MARK: Interactor Output (Interactor -> Presenter)
protocol InteractorToPresenterSubscribeProtocol {
    
    func subscribeDidComplete(status: Bool)
}

// MARK: Router Input (Presenter -> Router)
protocol PresenterToRouterSubscribeProtocol {
    func presentErrorSubscriptionModule(from: UIViewController)
    func presentSuccessSubscriptionModule(from: UIViewController)
}


struct SubscriptionData {
    let name: String
    let cardNumber: Int
    let expireMonth: Int
    let expireYear: Int
}
