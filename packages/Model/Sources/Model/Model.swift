import CoreData

public protocol DatabaseTransformer {
    func transform(object: NSManagedObject)
}

public struct Model {
    public private(set) var text = "Hello, World!"

    public init() {
    }
}
