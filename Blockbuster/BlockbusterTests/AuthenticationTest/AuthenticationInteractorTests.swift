//
//  AuthenticationInteractorTest.swift
//  BlockbusterTests
//
//  Created by Yanet Rodriguez on 21/11/2022.
//

@testable import Blockbuster
import Foundation
import XCTest

class AuthenticationInteractorTests: XCTestCase {
    
    // MARK: Subject under test
    
    var authInteractor: AuthenticationInteractor!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupAuthenticationInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupAuthenticationInteractor() {
        authInteractor = AuthenticationInteractor()
    }
    
    // MARK: Test doubles
    
    class AuthenticationPresentationLogicSpy: AuthenticationPresentationLogic {
        var presentLoginCalled = false
        var presentLoginResponse: Authentication.Login.Response!
        
        func presentLogin(response: Authentication.Login.Response) {
            presentLoginCalled = true
            presentLoginResponse = response
        }
    }
    
    // MARK: Tests
    
    // MARK: Dummy
    
    class AuthenticationWorkerDummy: AuthenticationWorker {
        override func login(email: String, password: String, completionHandler: @escaping (Bool) -> Void) {
            completionHandler(false)
        }
    }
    
    func testLoginShouldAskPresenterToFormatResult() {
        // Given
        let authWorkerDummy = AuthenticationWorkerDummy()
        authInteractor.worker = authWorkerDummy
        let authPresentationLoginSpy = AuthenticationPresentationLogicSpy()
        authInteractor.presenter = authPresentationLoginSpy
        let request = Authentication.Login.Request(email: "any", password: "any")
        
        // When
        authInteractor.login(request: request)
        
        // Then
        XCTAssertTrue(authPresentationLoginSpy.presentLoginCalled, "login(request:) should ask the presenter to format the result")
    }
    
    // MARK: Stub
    
    class AuthenticationWorkerStub: AuthenticationWorker {
        override func login(email: String, password: String, completionHandler: @escaping (Bool) -> Void)
        {
            completionHandler(true)
        }
    }
    
    // MARK: Spy
    
    class AuthenticationWorkerSpy: AuthenticationWorker {
        var loginCalled = false
        
        override func login(email: String, password: String, completionHandler: @escaping (Bool) -> Void)
        {
            loginCalled = true
            completionHandler(true)
        }
    }
    
    // MARK: Mock
    
    class AuthenticationWorkerMock: AuthenticationWorker {
        var loginCalled = false
        
        func verifyLoginIsCalled() -> Bool
        {
            return loginCalled
        }
        
        override func login(email: String, password: String, completionHandler: @escaping (Bool) -> Void)
        {
            loginCalled = true
            completionHandler(true)
        }
    }
    
    // MARK: Fake
    
    class AuthenticationWorkerFake: AuthenticationWorker {
        override func login(email: String, password: String, completionHandler: @escaping (Bool) -> Void)
        {
            if email == "good" && password == "good" {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func testLoginShouldNotPassUsernameToPresenterOnFailure() {
        // Given
        let authWorkerFake = AuthenticationWorkerFake()
        authInteractor.worker = authWorkerFake
        let authPresentationLogicSpy = AuthenticationPresentationLogicSpy()
        authInteractor.presenter = authPresentationLogicSpy
        let request = Authentication.Login.Request(email: "bad", password: "bad")
        
        // When
        authInteractor.login(request: request)
        
        // Then
        XCTAssertFalse(authPresentationLogicSpy.presentLoginResponse.success, "login(request:) with invalid email and password should result in failure")
    }
}
