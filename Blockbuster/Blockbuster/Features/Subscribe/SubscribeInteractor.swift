//
//  SubscribeInteractor.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 16/11/22.
//  
//

import Foundation
import APIService

class SubscribeInteractor: PresenterToInteractorSubscribeProtocol {

    // MARK: Properties
    var presenter: InteractorToPresenterSubscribeProtocol?
    
    func requestSubscription(with subscriptionData: SubscriptionData) {
        //NETWORKING HERE
        networkOperation { response in
            self.presenter?.subscribeDidComplete(status: response)
        }
    }
    
    func networkOperation(response: @escaping (Bool) -> ()) {
        
    }
}
