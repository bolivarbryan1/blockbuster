//
//  MoviesInteractorTest.swift
//  BlockbusterTests
//
//  Created by u633174 on 15/11/2022.
//

import XCTest
@testable import Blockbuster
import Model


final class MoviesInteractorTest: XCTestCase {

    var sut: MoviesInteractor!
    var sutPresenter: MoviesPresenter!
    var sutWorker: MoviesAPIWorker!
    var movies: Movie!
    
    override func setUpWithError() throws {
        sut = MoviesInteractor()
        sutWorker = MoviesAPIWorker()
        sutPresenter = MoviesPresenter()
        movies = Movie(name: "testName", description: "someDescription", movieID: 4)
    }

    override func tearDownWithError() throws {
        sut = nil
        sutWorker = nil
        sutPresenter = nil
        movies = nil
    }

    func testSearchBarInteractor() {
        let request = Movies.Search.Request(query: "Back")
        
        sut.searchMovie(request: request)
        
        XCTAssertFalse(sutWorker.searchMovieCalled, "searchMovie should call the worker searchMovie")
    }
    func testFetchMovieListInteractor() {
        let request = Movies.MovieList.Request()
        sut.fetchMovieList(request: request)
        sutWorker.fetchMovieList { movie in
            XCTAssertNotNil(movie)
            XCTAssertNotNil(movie.results.first?.name)
            XCTAssertTrue(movie.results.first?.movieID != 0)
        } failure: {
            
        }

        XCTAssertTrue(sutWorker.featchMovieCalled, "featchMovieList should call the worker featchMovieList")
        XCTAssertFalse(sutPresenter.presentMovieListCalled)

        
    }
}

