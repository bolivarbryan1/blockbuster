//
//  MovieDetailsViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 21/10/22.
//

import UIKit
import Alamofire
import Model


class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var viewModel: MoviesViewModel?
    var movie: Movie?
    
    var isFavorite: Bool {
        guard let movie = movie,
              let isFavorite = viewModel?.isFavoriteMovie(movie)
        else {
            return false
        }
        
        return isFavorite
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewComponents()
        configureFavoriteButton()
    }
    
    func setupViewComponents() {
        guard let movie = movie else {
            return
        }
        
        nameLabel.text = movie.name
        //genreLabel.text = movie.genre.rawValue
        movieImageView.kf.setImage(with: movie.imageURLRepresentation())
    }
    
    @IBAction func makeFavorite(_ sender: Any) {
        toggleMovieAsFavorite()
    }
    
    func toggleMovieAsFavorite() {
        guard let movie = movie else {
            return
        }
        
        if isFavorite {
            viewModel?.deleteMovie(movie)
        } else {
            viewModel?.saveMovieAsFavorite(movie)
        }
        configureFavoriteButton()
    }
    
    func configureFavoriteButton() {
        if isFavorite {
            favoriteButton.setTitle("Unfavorite", for: .normal)
        } else {
            favoriteButton.setTitle("Favorite", for: .normal)
        }
    }
}
