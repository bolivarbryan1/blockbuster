//
//  PosterCollectionViewCell.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/10/22.
//

import UIKit
import Kingfisher
import Model

class PosterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: UIImageView!

    var movie: Movie? {
        didSet {
            guard let movie = movie else {
                return
            }
            movieImageView.kf.setImage(with:movie.imageURLRepresentation())
        }
    }
}
