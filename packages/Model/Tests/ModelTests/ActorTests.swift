//
//  File.swift
//  
//
//  Created by Bryan A Bolivar M on 11/11/22.
//

import Foundation
import XCTest
@testable import Model

final class ActorTests: XCTestCase {
    
    func testImageURLRepresentationShouldBeDefault() {
        let actor = Actor(name: "Bryan", popularity: 1, id: 1)
        XCTAssertEqual(actor.imageURLRepresentation()?.absoluteURL, Constants.defaultImageURL.absoluteURL)
    }
    
    func testActorDictionaryRepresentationContainsAllKeys() {
        let actor = Actor(name: "Bryan", popularity: 1, id: 100)
        XCTAssertEqual(actor.dictionaryRepresentation.keys.count, 4)
    }
    
    func testActorBioShouldReturnADefaultImage() {
        let bio = ActorBio(birthday: nil, name: nil, biography: nil, popularity: nil, id: nil, profile_path: nil)
        XCTAssertNotNil(bio.imageURLRepresentation())
    }
}

