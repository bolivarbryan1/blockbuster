//
//  SubscribePresenter.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 16/11/22.
//  
//

import Foundation

class SubscribePresenter: ViewToPresenterSubscribeProtocol {


    // MARK: Properties
    var view: PresenterToViewSubscribeProtocol?
    var interactor: PresenterToInteractorSubscribeProtocol?
    var router: PresenterToRouterSubscribeProtocol?
    
    func requestSubscription(with subscriptionData: SubscriptionData) {
        interactor?.requestSubscription(with: subscriptionData)
    }
    
}

extension SubscribePresenter: InteractorToPresenterSubscribeProtocol {
    func subscribeDidComplete(status: Bool) {
        view?.showSubscriptionDetails(status: status)
    }
}
