import UIKit
import Model

protocol TvShowsCleanBusinessLogic
{
    func fetchTvShowsList(request: TvShowsClean.FetchTvShowsList.Request)
    func fetchFavoritesTvShowsList(request: TvShowsClean.FetchFavoritesTvShowsList.Request)
    func getSelectedTvShow(_ selectedShow: TvShow)
}

protocol TvShowsCleanDataStore
{
    var selectedTvShow: TvShow? {get set}
}

class TvShowsCleanInteractor: TvShowsCleanBusinessLogic, TvShowsCleanDataStore
{
  var presenter: TvShowsCleanPresentationLogic?
  var worker = TvShowsCleanWorker()
  var coreDataWorker = TvShowsCleanDBWorker()
  var tvShows: [TvShow] = []
  var selectedTvShow: TvShow?
  
    func getSelectedTvShow(_ selectedShow: TvShow){
        selectedTvShow = selectedShow
    }
  
  func fetchTvShowsList(request: TvShowsClean.FetchTvShowsList.Request) {
      worker.fetchTvShows(completion: { tvshows in
          DispatchQueue.main.async {
              let response = TvShowsClean.FetchTvShowsList.Response(tvshows: tvshows)
              self.presenter?.presentTvShowsLit(response: response)
          }
      }, failure: {})
  }
  
    func fetchFavoritesTvShowsList(request: TvShowsClean.FetchFavoritesTvShowsList.Request){
       let favorites = coreDataWorker.getTvShowsFavorites()
        let response = TvShowsClean.FetchFavoritesTvShowsList.Response(tvshows: favorites)
        self.presenter?.presentFavoritesTvShowList(response: response)
    }
}
