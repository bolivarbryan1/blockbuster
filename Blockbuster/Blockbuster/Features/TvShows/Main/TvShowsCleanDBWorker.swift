import UIKit
import Model

class TvShowsCleanDBWorker
{
    func getTvShowsFavorites() -> [TvShow] {
        let tvShows = DatabaseManager.shared.fetch(entity: .tvShow, queryType: .all).map({ object -> TvShow in
            TvShow(name: object.value(forKey: "name") as! String,
                   description: object.value(forKey: "about") as! String,
                   image: object.value(forKey: "imageURL") as? String,
                   rating: (object.value(forKey: "rating") as! NSString).doubleValue)
        })
        return tvShows
    }
}
