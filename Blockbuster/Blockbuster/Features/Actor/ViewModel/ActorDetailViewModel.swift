//
//  ActorDetailViewModel.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 01/11/2022.
//

import Foundation
import Alamofire
import APIService
import Moya
import APIService
import Model

class ActorDetailViewModel: ObservableObject {
    
    private var actors: [Actor] = []
    var isFavoritesEnabled: Bool = false
    
    @Published var actorDetail : ActorBio?
    
    func actoDetail() -> ActorBio?{
        return actorDetail
    }
    
    @objc func fetchActorDetail(withId id: Int,completion: @escaping () -> (), failure: @escaping () -> ()){
        
        let provider = MoyaProvider<TheMovieDBAPI>()
        
        provider.request(.actorDetail(id)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let actorDetail = try JSONDecoder().decode(ActorBio.self, from: moyaResponse.data)
                    self.actorDetail = actorDetail
                    completion()
                } catch {
                    failure()
                }
            case let .failure(error):
                print("Error Actores: \(error)")
                failure()
            }
        }
    }

}
