//
//  ActorBio.swift
//  
//
//  Created by Cristian Gonzalez on 01/11/2022.
//

import Foundation
import UIKit

public struct ActorBio: Codable {
    
    public let birthday: String?
    public let name: String?
    public let biography: String?
    public let popularity: Double?
    public let id: Int?
    public let profile_path: String?
    public var isFavorite: String?
    
    enum CodeingKeys: String, CodingKey{
        case birthday  = "birthday"
        case name = "name"
        case biography = "bography"
        case popularity = "popularity"
        case id = "id"
        case profile_path = "profile_path"
    }
}

extension ActorBio: ImageRepresentable {
    public var path: String? {
        return profile_path
    }
}
