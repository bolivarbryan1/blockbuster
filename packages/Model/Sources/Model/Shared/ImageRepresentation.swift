//
//  File.swift
//  
//
//  Created by Bryan A Bolivar M on 11/11/22.
//

import Foundation

public protocol ImageRepresentable {
    var path: String? { get }
    func imageURLRepresentation() -> URL?
}

extension ImageRepresentable {
    public func imageURLRepresentation() -> URL? {
        guard
            let image = path,
            !image.isEmpty
        else {
            return Constants.defaultImageURL
        }
        return URL(string:  "https://image.tmdb.org/t/p/w500/" + image)
    }
}
