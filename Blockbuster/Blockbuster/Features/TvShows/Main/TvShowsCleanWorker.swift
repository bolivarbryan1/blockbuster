import UIKit
import Moya
import APIService
import Model

struct TvShowsListResponse: Codable {
    let page: Int
    let results: [TvShow]
}

class TvShowsCleanWorker
{
    func fetchTvShows(completion: @escaping ([TvShow]) -> (), failure: @escaping () -> ())
    {
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.tvshows){ result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let tvshows = try JSONDecoder().decode(TvShowsListResponse.self, from: moyaResponse.data)
                    completion(tvshows.results)
                } catch {
                    print(error)
                    failure()
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure()
                
            }
        }
    }
}
