//
//  ActorViewModel.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 26/10/2022.
//

import Foundation
import Alamofire
import Moya

class ActorViewModel{
    
    var actores : [Actor] = []
    
    @objc func fetchActors(completion: @escaping () -> (), failure: @escaping () -> ()){
        
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.actorpopular) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    print(result)
                    let actors = try JSONDecoder().decode(ActorListResponse.self, from: moyaResponse.data)
                    self.actores = actors.results
                    completion()
                } catch {
                    print(error)
                    failure()
                }
            case let .failure(error):
                print("Error Actores: \(error)")
                print(error.localizedDescription)
                failure()
            }
        }
    }
}
