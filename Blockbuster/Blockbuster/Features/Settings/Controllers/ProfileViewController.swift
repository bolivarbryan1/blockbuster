//
//  SettingsViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 26/10/22.
//

import UIKit
import SnapKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let signOutButton = UIButton(frame: .zero)
        view.addSubview(signOutButton)
        signOutButton.setTitle("Sign Out", for: .normal)
        signOutButton.setTitleColor(.red, for: .normal)
        
        signOutButton.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(40)
            make.centerY.equalToSuperview()
        }
    }

}
