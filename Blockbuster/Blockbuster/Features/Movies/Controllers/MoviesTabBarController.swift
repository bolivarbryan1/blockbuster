//
//  HomeTabBarController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 28/10/22.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers([
            generateMoviesController(),
            generatePostersController(),
            generateActorsController(),
            generateTvShowsController()
        ], animated: true)
    }
    
    func generateActorsCleanArchController() -> UINavigationController {
        let vc = UIStoryboard(name: "ActorCleanArchitectureStoryboard", bundle: nil).instantiateViewController(withIdentifier: "ActorCleanArchictectureController") as! ActorCleanArchictectureController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 2)
        navigationController.tabBarItem.title = "Actor"
        navigationController.title = "Actors"
        vc.title = "Actors List"
        return navigationController
    }
    
    func generateActorsController() -> UINavigationController {
        let vc = UIStoryboard(name: "ActorsStoryboard", bundle: nil).instantiateViewController(withIdentifier: "ActorsViewController") as! ActorsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 2)
        navigationController.tabBarItem.title = "Actor"
        navigationController.title = "Actors"
        vc.title = "Actors List"
        vc.actorViewModel = ActorViewModel(loader: JSONActorsLoader())
        return navigationController
    }
    
    func generateTvShowsController() -> UINavigationController {
        let vc = TvShowsCleanViewController()
        let ncTvShows = UINavigationController.generate(controller: vc, image: UIImage(systemName: "person")!, title: "Tv Shows")
        return ncTvShows
    }
    
    
    func generateMoviesController() -> UINavigationController {
        //let vc = UIStoryboard(name: "MoviesStoryboard", bundle: nil).instantiateViewController(withIdentifier: "MoviesViewController") as! MoviesViewController
        let vc = UIStoryboard.generateController(type: MoviesViewController.self, storyBoard: "MoviesStoryboard", identifier: "MoviesViewController")
        
        let ncTvShows = UINavigationController.generate(controller: vc, image: UIImage(systemName: "person")!, title: "Movies")
        return ncTvShows
    }
    
    
    func generatePostersController() -> UINavigationController {
        let vc = UIStoryboard(name: "MoviesStoryboard", bundle: nil).instantiateViewController(withIdentifier: "PostersViewController") as! PostersViewController
        let ncTvShows = UINavigationController.generate(controller: vc, image: UIImage(systemName: "person")!, title: "Posters")
        return ncTvShows
    }
}

extension UIStoryboard {
    static func generateController<T: UIViewController>(type: T.Type ,storyBoard: String, identifier: String) -> T {
        return UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: identifier) as! T
    }
}
