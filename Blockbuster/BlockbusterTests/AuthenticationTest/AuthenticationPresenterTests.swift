//
//  AuthenticationPresenterTests.swift
//  BlockbusterTests
//
//  Created by Yanet Rodriguez on 21/11/2022.
//

@testable import Blockbuster
import Foundation
import XCTest

class AuthenticationPresenterTests: XCTestCase {
    
    // MARK: Subject under test
    
    var authPresenter: AuthenticationPresenter!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupAuthenticationPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupAuthenticationPresenter() {
        authPresenter = AuthenticationPresenter()
    }
    
    // MARK: Test doubles
    
    class AuthenticationDisplayLogicSpy: AuthenticationDisplayLogic {
        var displayLoginCalled = false
        var displayLoginViewModel: Authentication.Login.ViewModel!
        
        func displayLogin(viewModel: Authentication.Login.ViewModel)
        {
            displayLoginCalled = true
            displayLoginViewModel = viewModel
        }
    }
    
    // MARK: Tests
    
    func testPresentLoginOnSuccess() {
        // Given
        let authDisplayLogicSpy = AuthenticationDisplayLogicSpy()
        authPresenter.viewController = authDisplayLogicSpy
        let response = Authentication.Login.Response(success: true)
        
        // When
        authPresenter.presentLogin(response: response)
        
        // Then
        XCTAssertTrue(authDisplayLogicSpy.displayLoginCalled, "presentLogin(response:) should ask the view controller to display the result")
        XCTAssertTrue(authDisplayLogicSpy.displayLoginViewModel.success, "presentLogin(response:) should result in success")
    }
    
    func testPresentLoginOnFailure() {
        // Given
        let authDisplayLogicSpy = AuthenticationDisplayLogicSpy()
        authPresenter.viewController = authDisplayLogicSpy
        let response = Authentication.Login.Response(success: false)
        
        // When
        authPresenter.presentLogin(response: response)
        
        // Then
        XCTAssertTrue(authDisplayLogicSpy.displayLoginCalled, "presentLogin(response:) should ask the view controller to display the result")
        XCTAssertFalse(authDisplayLogicSpy.displayLoginViewModel.success, "presentLogin(response:) should result in failure")
    }
}
