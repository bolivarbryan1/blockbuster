//
//  SubscribeRouter.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 16/11/22.
//  
//

import Foundation
import UIKit

class SubscribeRouter: PresenterToRouterSubscribeProtocol {
    
    // MARK: Static methods
    static func createModule() -> UIViewController {
        
        let viewController = SubscribeViewController()
        
        let presenter: ViewToPresenterSubscribeProtocol & InteractorToPresenterSubscribeProtocol = SubscribePresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = SubscribeRouter()
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = SubscribeInteractor()
        viewController.presenter?.interactor?.presenter = presenter
        
        return viewController
    }
    
    func presentErrorSubscriptionModule(from: UIViewController) {
        //present error subscription module
    }
    
    func presentSuccessSubscriptionModule(from: UIViewController) {
        //present success subscription module
    }
    
}
